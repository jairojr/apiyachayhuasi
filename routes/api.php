<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::apiResource('/personas', \App\Http\Controllers\API\PersonaController::class)->except('show');

Route::apiResource('curso', \App\Http\Controllers\API\CursoController::class);

Route::apiResource('/colegio', \App\Http\Controllers\ColegioController::class);

Route::apiResource('/aula', \App\Http\Controllers\AulaController::class)->except('index');
Route::get('/aula/colegio/{idColegio}', [\App\Http\Controllers\AulaController::class, 'index']);

// Route::apiResource('/alumno', \App\Http\Controllers\AlumnoController::class)->except('index');
Route::post('/alumno', [\App\Http\Controllers\AlumnoController::class,'store']);
Route::get('/alumno/list/{idColegio}/{idAula}', [\App\Http\Controllers\AlumnoController::class, 'index']);

Route::get('/personas/credential/{id}', [\App\Http\Controllers\API\PersonaController::class, 'showCredential']);

Route::get('/personas/role/{role}', [\App\Http\Controllers\API\PersonaController::class, 'indexUsersByRole']);

/**
 *  Rutas de Autenticacion
 */
Route::post('/login', [\App\Http\Controllers\AuthController::class,'login']);
Route::middleware('auth:sanctum')->group(function (){
    Route::get('/personas/profile', [\App\Http\Controllers\API\PersonaController::class,'show']);
    Route::get('/logout', [\App\Http\Controllers\AuthController::class,'logout']);

});
