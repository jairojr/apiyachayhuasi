<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ColegioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Reglas de validacion
            'id_admin' => 'required',
            'name' => 'required',
            'description' => '',
            'phone' => 'required',
            'email' => 'required',
            'address' => 'required',
            'url_image' => ['required', 'image']
            // 'url_image' => 'required'
        ];
    }
}
