<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CursoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Reglas de validacion
            'course_type' => ['required', 'numeric'],
            'title' => 'required',
            'category' => 'required',
            'level' => 'required',
            'state' => ['required', 'numeric'],
            'duration' => 'required'
        ];
    }
}
