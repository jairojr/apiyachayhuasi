<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActualizarPersonaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Reglas de validacion del request
            "avatar" => "required",
            "first_name" => "required",
            "last_name" => "required",
            "gender" => ['required','max:6'],
            "date_of_birth" => ["required","max:11"],
            "city" => "required",
            "role" => "required",
            "job_title" => "required",
            "skill" => "required",
            "company" => "required",
            "address" => "required",
            "phone" => ["required","max:19"],
            "website" => "required",
            "email" => "required",
        ];
    }
}
