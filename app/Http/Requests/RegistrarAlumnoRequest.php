<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrarAlumnoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Reglas de validacion del request
            'avatar' => 'url',
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'max:6',
            'doc_number' => 'required',
            'phone' => ['required','max:19'],
            'date_of_birth' => 'max:11',
            'country' => '',
            'region' => '',
            'city' => '',
            'address' => '',
            'role' => '',
            'skill' => '',
            'job_title' => '',
            'company' => '',
            'date_job' => 'max:11',
            'website' => '',
            'email' => ['required','unique:personas,email'],
            'password' => 'required',
            'id_colegio' => ['required', 'exists:aulas,id_colegio'],
            'id_aula' => ['required', 'exists:aulas,id']
        ];
    }
}
