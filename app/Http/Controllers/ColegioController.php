<?php

namespace App\Http\Controllers;

use App\Http\Requests\ColegioRequest;
use App\Http\Resources\ColegioResource;
use App\Models\Colegio;
use App\Models\Persona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ColegioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Retorna todos los colegios
        return ColegioResource::collection(Colegio::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ColegioRequest $colegioRequest
     * @return \Illuminate\Http\Response
     */
    public function store(ColegioRequest $colegioRequest)
    {
        $colegio = $colegioRequest->all();
        $extension = $colegioRequest->url_image->extension();

        $newNameFile = str_replace('.', '', $colegioRequest->name);
        $newNameFile = str_replace(' ', '_', $newNameFile);
        $newNameFile = strtolower($newNameFile).'.'.$extension;

        /**
         * 01
        */
        // $colegio['url_image'] = $colegioRequest->file('url_image')->store('/colegios');
        // $colegio['url_image'] = Storage::putFile('colegios', $colegioRequest->file('url_image'));

            /**
         * 02
         */
        // $nameOriginal = time().'_'. $colegioRequest->file('url_image')->getClientOriginalName();
        // $colegio['url_image'] = $colegioRequest->file('url_image')->storeAs('colegios', $nameOriginal);

        $urlBase = env('APP_URL').'/storage/';
        $colegio['url_image'] = $urlBase.Storage::putFileAs('colegios', $colegioRequest->file('url_image'), $newNameFile);

        $newColegio = Colegio::create($colegio);
        // $colegio = Colegio::create($colegioRequest->all());

        return response()->json([
            'message' => 'Colegio creado',
            'colegio' => $newColegio
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function show(Colegio $colegio)
    {
        $colegioData = $colegio;
        // Datos del administrador
        $idAdmin = $colegio->id_admin;
        $dataAdmin = Persona::where('id', $colegio->id_admin)->first();

        // Datos de un colegio
        $colegioData['name_admin'] = $dataAdmin['first_name'].' '.$dataAdmin['last_name'];
        $colegioData['avatar_admin'] = $dataAdmin['avatar'];

        // return new ColegioResource($colegio);

        return response()->json([
            'message' => 'Datos del colegio',
            'data' => $colegioData
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function update(ColegioRequest $colegioRequest, Colegio $colegio)
    {
        // Actualizar datos de colegio
        $colegio->update($colegioRequest->all());
        return response()->json([
            'message' => 'Datos actualizados'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Colegio $colegio)
    {
        // Obtencion del nombre del archivo
        /*$nameFile = explode('/', $colegio->url_image);
        $tamArray = count($nameFile);
        $nameFile = $nameFile[$tamArray-1];*/

        // Eliminar el colegio de la BD
        $colegio->delete();

        // Elimnar el archivo del storage
        // Storage::delete('colegios/'.$nameFile);

        return response()->json([
            'message' => 'Colegio eliminado'
        ]);
    }
}
