<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrarAlumnoRequest;
use App\Models\Alumno_colegio;
use App\Models\Persona;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class AlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  int  $idColegio
     * @param  int  $idAula
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index($idColegio,$idAula)
    {
        if ($idAula == 0){
            $resultDB = DB::table('alumno_colegios')
                ->join('aulas','alumno_colegios.id_aula','=','aulas.id')
                ->join('personas','alumno_colegios.id_alumno','=','personas.id')
                ->where('alumno_colegios.id_colegio','=',$idColegio)
                ->select('alumno_colegios.id_alumno','personas.avatar','personas.first_name','personas.last_name','aulas.level','aulas.grade','aulas.section')
                ->paginate(10);

            return $resultDB;
        }else{
            $resultDB = DB::table('alumno_colegios')
                ->join('aulas','alumno_colegios.id_aula','=','aulas.id')
                ->join('personas','alumno_colegios.id_alumno','=','personas.id')
                ->where('alumno_colegios.id_colegio','=',$idColegio)
                ->where('alumno_colegios.id_aula','=',$idAula)
                ->select('alumno_colegios.id_alumno','personas.avatar','personas.first_name','personas.last_name','aulas.level','aulas.grade','aulas.section')
                ->paginate(10);

            return $resultDB;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RegistrarAlumnoRequest $request
     * @return JsonResponse
     */
    public function store(RegistrarAlumnoRequest $request)
    {
        try {
            // Registro en la tabla personas
            $persona = Persona::create($request->except(['id_colegio', 'id_aula']));

            // Registro en la tabla users
            $user = new User();
            $user->idPersona = $persona->id;
            $user->email = $request->email;

            $user->password = Crypt::encrypt($request->password);;
            $user->save();

            // Resgistro en la tabla alumno_colegios
            $newAlumno = new Alumno_colegio();
            $newAlumno->id_alumno = $persona->id;
            $newAlumno->id_colegio = $request->id_colegio;
            $newAlumno->id_aula = $request->id_aula;
            $newAlumno->save();
        } catch (\Exception $error){
            return response()->json([
                'statusCode' => 400,
                'error' => $error
            ],400);
        }

        return response()->json([
            'statusCode' => 200,
            'message' => 'Alumno guardado correctamente',
            'persona' => $persona,
            'alumno' => $newAlumno
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
