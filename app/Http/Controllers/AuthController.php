<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\Persona;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Crear nuevo usuario
    */
    public function signup(){

    }

    /**
     *  Iniciar sesion
     *
     */
    public function login(LoginRequest $request){
        $user = User::where('email',$request->email)->first();

        $validPassword = ($request->password == Crypt::decrypt($user->password)) ? true : false;

        // if (!$user || !Hash::check($request->password, $user->password)){
        if (!$user || $validPassword){
            throw ValidationException::withMessages([
                'statusCode' => 204,
                'message' => ['Invalid login or password.']
            ]);
        }
        $persona = Persona::where('id',$user->idPersona)->first();
        $token = $user->createToken($request->email)->plainTextToken;
        return response()->json([
            'statusCode' => 200,
            'token' => $token,
            'persona' => $persona->only(['avatar','first_name','last_name'])
        ],200);
    }

    /**
     *  Cerrar sesion
     *
     */
    public function logout(Request $request){
        $request->user()->currentAccessToken()->delete();
        return response()->json([
            'statusCode' => 200,
            'message' => 'Token delete successful',
            'data' => $request->user()
        ]);
    }
}
