<?php

namespace App\Http\Controllers;

use App\Http\Requests\AulaRequest;
use App\Http\Resources\AulaResource;
use App\Models\Aula;
use Illuminate\Http\Request;

class AulaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($idColegio)
    {
        //
        $aulasToColegio = Aula::where('id_colegio', $idColegio)->get();
        return AulaResource::collection($aulasToColegio);

        /*return response()->json([
            'statusCode' => 200,
            'request' => $aulasToColegio
        ], 200);*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AulaRequest $request)
    {
        //
        $newAula = Aula::create($request->all());
        return response()->json([
            'message' => 'Aula creado',
            'aula' => $newAula
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Aula  $aula
     * @return \Illuminate\Http\Response
     */
    public function show(Aula $aula)
    {
        //
        return new AulaResource($aula);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Aula  $aula
     * @return \Illuminate\Http\Response
     */
    public function update(AulaRequest $request, Aula $aula)
    {
        //
        $aula->update($request->all());
        return response()->json([
            'response' => true,
            'message' => 'Aula actualizado correctamente',
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Aula  $aula
     * @return \Illuminate\Http\Response
     */
    public function destroy(Aula $aula)
    {
        //
        $aula->delete();
        return response()->json([
            'response' => true,
            'message' => 'Aula eliminado correctamente',
        ], 200);
    }
}
