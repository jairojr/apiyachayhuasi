<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CursoRequest;
use App\Models\Curso;
use App\Models\Dirigido;
use App\Models\Entrenamiento;
use Illuminate\Http\Request;

class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CursoRequest $cursoRequest)
    {
        // Registro de un curso
        $curso = Curso::create($cursoRequest->except('course_type'));

        $entrenamiento = null;
        $dirigido = null;
        // validar el tipo de curso
        if ($cursoRequest->course_type == 1){
            // curso del tipo entrenamiento
            $entrenamiento = new Entrenamiento();
            $entrenamiento->idCurso = $curso->id;
            $entrenamiento->save();
        } else {
            // curso del tipo dirigido
            $dirigido = new Dirigido();
            $dirigido->idCurso = $curso->id;
            $dirigido->save();
        }

        return response()->json([
            'curso' => $curso,
            'entrenamiento' => $entrenamiento,
            'dirigido' => $dirigido
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
