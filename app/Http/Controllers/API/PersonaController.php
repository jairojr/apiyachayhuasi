<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ActualizarPersonaRequest;
use App\Http\Requests\RegistrarPersonaRequest;
use App\Http\Resources\PersonaResource;
use App\Models\Persona;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Retorna todos los registros
        // return PersonaResource::collection(Persona::all(['id','avatar','first_name','last_name','email','role']));
        return Persona::paginate(10,['id','avatar','first_name','last_name','email','role']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegistrarPersonaRequest $registrarPersonaRequest)
    {
        // Registro de un recurso en la tabla personas
        $persona = Persona::create($registrarPersonaRequest->except('password'));

        // Registro en la tabla users
        $user = new User();
        $user->idPersona = $persona->id;
        $user->email = $registrarPersonaRequest->email;
        // $user->password = bcrypt($registrarPersonaRequest->password);
        $user->password = Crypt::encrypt($registrarPersonaRequest->password);;
        $user->save();

        return response()->json([
            'statusCode' => 200,
            'message' => 'Persona guardado correctamente'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $response = $request->user()->currentAccessToken();
        $idPersona = $response->tokenable->idPersona;

        $dataProfile = Persona::where('id',$idPersona)->first();

        // Muestra un recurso en especifico
        return response()->json([
            'statusCode' => 200,
            'data' => $dataProfile
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ActualizarPersonaRequest $actualizarPersonaRequest, Persona $persona)
    {
        // Actualiza un recurso en especifico
        $persona->update($actualizarPersonaRequest->all());
        return response()->json([
            'response' => true,
            'message' => 'Persona actualizado correctamente',
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Persona $persona)
    {
        // Elimina un recurso en especifico
        $persona->delete();
        return response()->json([
            'response' => true,
            'message' => 'Persona eliminado correctamente',
        ], 200);
    }


    /**
     * View credentials of user specified
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function showCredential(Request $request)
    {
        $user = User::where('idPersona', $request->id)->first();
        $email = $user->email;
        $pswdCryp = $user->password;
        $pswdDecryp = Crypt::decrypt($pswdCryp);

        $persona = Persona::where('id', $request->id)->first();
        $avatar = $persona->avatar;
        $nombre = $persona->first_name.' '.$persona->last_name;
        return response()->json([
            'response' => true,
            'avatar' => $avatar,
            'nombre' => $nombre,
            'email' => $email,
            'password' => $pswdDecryp
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function indexUsersByRole(Request $request)
    {
        $user = Persona::where('role', $request->role)->get();
        /*$email = $user->email;
        $pswdCryp = $user->password;
        $pswdDecryp = Crypt::decrypt($pswdCryp);*/

        /*$persona = Persona::where('id', $request->id)->first();
        $avatar = $persona->avatar;
        $nombre = $persona->first_name.' '.$persona->last_name;*/
        /*return response()->json([
            'response' => true,
            'avatar' => $avatar,
            'nombre' => $nombre,
            'email' => $email,
            'password' => $pswdDecryp
        ], 200);*/
        return response()->json([
            'response' => $user
        ], 200);
    }
}
