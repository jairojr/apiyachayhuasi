<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Colegio extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_admin',
        'name',
        'description',
        'phone',
        'email',
        'address',
        'url_image'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

}
