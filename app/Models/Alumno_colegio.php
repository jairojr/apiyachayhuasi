<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alumno_colegio extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_alumno',
        'id_colegio',
        'id_aula'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
