<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;

    protected $fillable = [
        'avatar',
        'first_name',
        'last_name',
        'gender',
        'doc_number',
        'phone',
        'date_of_birth',
        'country',
        'region',
        'city',
        'address',
        'role',
        'skill',
        'job_title',
        'company',
        'date_job',
        'website',
        'email'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
