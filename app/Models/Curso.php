<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'category',
        'level',
        'state',
        'duration',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
