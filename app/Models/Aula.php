<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aula extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_colegio',
        'level',
        'section',
        'grade',
        'description',
        'url_image'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
