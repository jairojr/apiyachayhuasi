<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->id();
            $table->string('avatar')->default('https://robohash.org/lapichula.png?size=128x128&set=set3');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('gender',6)->nullable();
            $table->string('doc_number');
            $table->string('phone',19);
            $table->string('date_of_birth',11)->nullable();
            $table->string('country')->nullable();
            $table->string('region')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('role')->nullable();
            $table->string('skill')->nullable();
            $table->string('job_title')->nullable();
            $table->string('company')->nullable();
            $table->string('date_job', 11)->nullable();
            $table->string('website')->nullable();
            $table->string('email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
