<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlumnoColegiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumno_colegios', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_alumno')->unique()->constrained('personas');
            $table->foreignId('id_colegio')->index()->constrained('colegios');
            $table->foreignId('id_aula')->index()->constrained('aulas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumno_colegios');
    }
}
